﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.XImgproc;

namespace NorthPauldingRov
{
    public partial class bugForm : Form
    {
        private VideoCapture _capture;
        private Mat _frame;
        private List<Point> centers = new List<Point>();
        bool stopped;

        private void test(object sender, EventArgs e)
        {
            stopped = false;
        }

        private void ProcessFrame(object sender, EventArgs e)
        {
            if (_capture != null && _capture.Ptr != IntPtr.Zero && !stopped)
            {
                _capture.Retrieve(_frame, 0);
                imageBox1.Image = _frame;
            }
        }

        public bugForm()
        {
            InitializeComponent();
            stopped = false;
            _capture = new VideoCapture(0);

            _capture.ImageGrabbed += ProcessFrame;
            _frame = new Mat();
            if (_capture != null)
            {
                try
                {
                    _capture.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void bugForm_Load(object sender, EventArgs e)
        {

        }

        private void Freeze_Click(object sender, EventArgs e)
        {
            if (Freeze.Text == "Freeze")
            {
                Freeze.Text = "Unfreeze";
                _capture.Stop();
                Image<Bgr, Byte> img = new Image<Bgr, Byte>(imageBox1.Image.Bitmap);
                UMat uimage = new UMat();
                CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);
                img = uimage.ToImage<Bgr, Byte>();
                img = img.PyrDown().PyrUp().SmoothMedian(5);
                imageBox2.Image = img;
                stopped = true;

                double resolution = 2.0;
                double minDistance = 20.0;
                int cannyThreshold = 180;
                int accumulatorThreshold = 90;
                int minRadius = 5;
                int maxRadius = 500;

                Image<Bgr, Byte> img2 = new Image<Bgr, byte>(img.Bitmap);

                //apply OpenCV Hough Transform Circle Detection Algorithm
                Color cannyColor = Color.FromArgb(1, cannyThreshold, cannyThreshold, cannyThreshold);
                Color threshold = Color.FromArgb(1, accumulatorThreshold, accumulatorThreshold, accumulatorThreshold);
                CircleF[][] circles = img2.HoughCircles(new Bgr(cannyColor), new Bgr(threshold), resolution, minDistance, minRadius, maxRadius);


                //Draw the circles on the image
                for (int i = 0; i < circles[0].Length; i++)
                {
                    Point center = new Point((int)circles[0][i].Center.X, (int)circles[0][i].Center.Y);
                    CvInvoke.Circle(img2, center, (int)circles[0][i].Radius, new MCvScalar(255, 0, 0), 3);
                    centers.Add(center);
                }

                label1.Text = "Circles: " + circles[0].Length;

                imageBox3.Image = img2;

                UMat cannyEdges; //New image to hold output from edge detection algorithm

                //setting for the Canny edge detection algorithm
                double threshold2 = 180.0;
                double thresholdLinking = 120.0;

                //apply OpenCV canny edge detection algorithm
                cannyEdges = img.Canny(threshold2, thresholdLinking).ToUMat();

                imageBox4.Image = cannyEdges.ToImage<Bgr, Byte>();

                VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint(); //Variable to hold output from the contour detection algorithm

                //apply OpenCV contour detection algorithm on the edge-detected image  Note: this aglorithm will find two copies of each shape (outer and inner)
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxNone);

                Image<Bgr, Byte> img3 = new Image<Bgr, byte>(img.Bitmap);

                for (int i = 0; i < contours.Size; i++)
                {
                    for (int j = 0; j < contours[i].Size; j++)
                    {
                        CvInvoke.Circle(img3, new Point(contours[i][j].X, contours[i][j].Y), 1, new MCvScalar(100, 100, 250), 1);
                    }
                }

                imageBox5.Image = img3;

                List<Triangle2DF> triangles = new List<Triangle2DF>();
                List<RotatedRect> rectangles = new List<RotatedRect>();
                List<RotatedRect> lines = new List<RotatedRect>();

                List<List<Point>> cornerList = new List<List<Point>>();

                for (int i = 0; i < contours.Size; i++)
                {
                    VectorOfPoint approxContour = new VectorOfPoint();
                    CvInvoke.ApproxPolyDP(contours[i], approxContour, CvInvoke.ArcLength(contours[i], true) * 0.04, true);

                    List<Point> corners = new List<Point>();

                    corners.Add(approxContour[0]);

                    for (int j = 1; j < approxContour.Size; j++)
                    {
                        int x1 = approxContour[j].X;
                        int y1 = approxContour[j].Y;
                        bool tooClose = false;
                        for (int k = 0; k < corners.Count; k++)
                        {
                            int x2 = corners[k].X;
                            int y2 = corners[k].Y;
                            double dist = Math.Sqrt((x1-x2)*(x1-x2) + (y1 - y2)*(y1-y2));
                            if (dist < 10)           //5, best so far
                            {
                                tooClose = true;
                            }
                        }
                        if (!tooClose)
                        {
                            corners.Add(approxContour[j]);
                        }
                    }

                    cornerList.Add(corners);

                    for (int j = 0; j < corners.Count; j++)
                    {
                        CvInvoke.Circle(img3, new Point(corners[j].X, corners[j].Y), 2, new MCvScalar(255, 0, 0), 2);
                    }

                    if (corners.Count == 3)
                    {
                        triangles.Add(new Triangle2DF(corners[0], corners[1], corners[2]));
                    }

                    if (corners.Count == 4)
                    {
                        Point[] vertices = corners.ToArray();
                        RotatedRect newRectangle = CvInvoke.MinAreaRect(new VectorOfPoint(corners.ToArray()));
                        
                        //Get the width and height of the rectangle
                        float width = newRectangle.Size.Width;
                        float height = newRectangle.Size.Height;

                        //Calculate the ratio of the smaller side to the larger side of the rectangle
                        float ratio = Math.Min(width, height) / Math.Max(width, height);

                        if (ratio > 0.3) //If the smaller side is at least 20% the size of the larger side, then count this as a rectangle
                        {
                            rectangles.Add(newRectangle);
                        }
                        else //otherwise count this as a line
                        {
                            lines.Add(newRectangle);
                        }
                    }
                }

                label2.Text = "Triangles: " + (triangles.Count - 1);
                label3.Text = "Rectangles: " + rectangles.Count / 2;
                label4.Text = "Lines: " + lines.Count / 2;
                


                imageBox6.Image = img3;

                //Draw the triangles on the image
                for (int i = 0; i < triangles.Count; i++)
                {
                    img.Draw(triangles[i], new Bgr(Color.Green), 3);
                }

                //Draw the rectangles on the image
                for (int i = 0; i < rectangles.Count; i++)
                {
                    img.Draw(rectangles[i], new Bgr(Color.Yellow), 3);
                }

                //Draw the lines on the image
                for (int i = 0; i < lines.Count; i++)
                {
                    img.Draw(lines[i], new Bgr(Color.Red), 3);
                }
                imageBox1.Image = img;

            } else
            {
                _capture.Start();
                Freeze.Text = "Freeze";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _capture.Start();
        }
    }
}