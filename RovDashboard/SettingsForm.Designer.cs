﻿namespace NorthPauldingRov
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.joystickTimer = new System.Windows.Forms.Timer(this.components);
            this.serialTimer = new System.Windows.Forms.Timer(this.components);
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.statusText = new System.Windows.Forms.TextBox();
            this.refreshButton = new System.Windows.Forms.Button();
            this.Calibrate = new System.Windows.Forms.Button();
            this.deviceList = new System.Windows.Forms.ListBox();
            this.textBoxCamera = new System.Windows.Forms.TextBox();
            this.refreshPorts = new System.Windows.Forms.Button();
            this.portsList = new System.Windows.Forms.ListBox();
            this.connectPort = new System.Windows.Forms.Button();
            this.textBoxPortStatus = new System.Windows.Forms.TextBox();
            this.videoBtn = new System.Windows.Forms.Button();
            this.bugForm2Btn = new System.Windows.Forms.Button();
            this.connect1Btn = new System.Windows.Forms.Button();
            this.connect2Btn = new System.Windows.Forms.Button();
            this.testBugBtn = new System.Windows.Forms.Button();
            this.settingsTemp = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // joystickTimer
            // 
            this.joystickTimer.Enabled = true;
            this.joystickTimer.Interval = 50;
            this.joystickTimer.Tick += new System.EventHandler(this.joystickTimer_Tick);
            // 
            // serialTimer
            // 
            this.serialTimer.Tick += new System.EventHandler(this.serialTimer_Tick);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // statusText
            // 
            this.statusText.BackColor = System.Drawing.SystemColors.Info;
            this.statusText.Location = new System.Drawing.Point(422, 230);
            this.statusText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.statusText.Name = "statusText";
            this.statusText.Size = new System.Drawing.Size(174, 26);
            this.statusText.TabIndex = 4;
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.Color.LightYellow;
            this.refreshButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.refreshButton.Location = new System.Drawing.Point(71, 40);
            this.refreshButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(319, 74);
            this.refreshButton.TabIndex = 0;
            this.refreshButton.Text = "Refresh Device List";
            this.refreshButton.UseVisualStyleBackColor = false;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // Calibrate
            // 
            this.Calibrate.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Calibrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Calibrate.Location = new System.Drawing.Point(422, 275);
            this.Calibrate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Calibrate.Name = "Calibrate";
            this.Calibrate.Size = new System.Drawing.Size(174, 73);
            this.Calibrate.TabIndex = 5;
            this.Calibrate.Text = "Calibrate";
            this.Calibrate.UseVisualStyleBackColor = false;
            this.Calibrate.Click += new System.EventHandler(this.Calibrate_Click);
            // 
            // deviceList
            // 
            this.deviceList.BackColor = System.Drawing.SystemColors.Info;
            this.deviceList.FormattingEnabled = true;
            this.deviceList.ItemHeight = 20;
            this.deviceList.Location = new System.Drawing.Point(71, 141);
            this.deviceList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deviceList.Name = "deviceList";
            this.deviceList.Size = new System.Drawing.Size(319, 284);
            this.deviceList.TabIndex = 1;
            this.deviceList.SelectedIndexChanged += new System.EventHandler(this.deviceList_SelectedIndexChanged);
            // 
            // textBoxCamera
            // 
            this.textBoxCamera.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxCamera.Location = new System.Drawing.Point(422, 373);
            this.textBoxCamera.Name = "textBoxCamera";
            this.textBoxCamera.Size = new System.Drawing.Size(174, 26);
            this.textBoxCamera.TabIndex = 7;
            // 
            // refreshPorts
            // 
            this.refreshPorts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.refreshPorts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshPorts.Location = new System.Drawing.Point(631, 40);
            this.refreshPorts.Name = "refreshPorts";
            this.refreshPorts.Size = new System.Drawing.Size(319, 73);
            this.refreshPorts.TabIndex = 8;
            this.refreshPorts.Text = "Refresh COM Ports";
            this.refreshPorts.UseVisualStyleBackColor = false;
            this.refreshPorts.Click += new System.EventHandler(this.refreshPorts_Click);
            // 
            // portsList
            // 
            this.portsList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.portsList.FormattingEnabled = true;
            this.portsList.ItemHeight = 20;
            this.portsList.Location = new System.Drawing.Point(631, 141);
            this.portsList.Name = "portsList";
            this.portsList.Size = new System.Drawing.Size(319, 284);
            this.portsList.TabIndex = 9;
            this.portsList.SelectedIndexChanged += new System.EventHandler(this.connectPort_Click);
            // 
            // connectPort
            // 
            this.connectPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.connectPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectPort.Location = new System.Drawing.Point(985, 40);
            this.connectPort.Name = "connectPort";
            this.connectPort.Size = new System.Drawing.Size(192, 75);
            this.connectPort.TabIndex = 10;
            this.connectPort.Text = "Connect Port";
            this.connectPort.UseVisualStyleBackColor = false;
            this.connectPort.Click += new System.EventHandler(this.connectPort_Click);
            // 
            // textBoxPortStatus
            // 
            this.textBoxPortStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxPortStatus.Location = new System.Drawing.Point(985, 141);
            this.textBoxPortStatus.Name = "textBoxPortStatus";
            this.textBoxPortStatus.Size = new System.Drawing.Size(192, 26);
            this.textBoxPortStatus.TabIndex = 11;
            this.textBoxPortStatus.TextChanged += new System.EventHandler(this.textBoxPortStatus_TextChanged);
            // 
            // videoBtn
            // 
            this.videoBtn.Location = new System.Drawing.Point(986, 198);
            this.videoBtn.Name = "videoBtn";
            this.videoBtn.Size = new System.Drawing.Size(191, 90);
            this.videoBtn.TabIndex = 12;
            this.videoBtn.Text = "Video Form";
            this.videoBtn.UseVisualStyleBackColor = true;
            this.videoBtn.Click += new System.EventHandler(this.videoBtn_Click);
            // 
            // bugForm2Btn
            // 
            this.bugForm2Btn.Location = new System.Drawing.Point(985, 316);
            this.bugForm2Btn.Name = "bugForm2Btn";
            this.bugForm2Btn.Size = new System.Drawing.Size(191, 83);
            this.bugForm2Btn.TabIndex = 13;
            this.bugForm2Btn.Text = "Bug Form";
            this.bugForm2Btn.UseVisualStyleBackColor = true;
            this.bugForm2Btn.Click += new System.EventHandler(this.bugFormBtn_Click);
            // 
            // connect1Btn
            // 
            this.connect1Btn.Location = new System.Drawing.Point(422, 40);
            this.connect1Btn.Name = "connect1Btn";
            this.connect1Btn.Size = new System.Drawing.Size(174, 75);
            this.connect1Btn.TabIndex = 14;
            this.connect1Btn.Text = "Connect 1";
            this.connect1Btn.UseVisualStyleBackColor = true;
            this.connect1Btn.Click += new System.EventHandler(this.connect1_Click);
            // 
            // connect2Btn
            // 
            this.connect2Btn.Location = new System.Drawing.Point(422, 138);
            this.connect2Btn.Name = "connect2Btn";
            this.connect2Btn.Size = new System.Drawing.Size(174, 75);
            this.connect2Btn.TabIndex = 15;
            this.connect2Btn.Text = "Connect 2";
            this.connect2Btn.UseVisualStyleBackColor = true;
            this.connect2Btn.Click += new System.EventHandler(this.connect2_Click);
            // 
            // testBugBtn
            // 
            this.testBugBtn.Location = new System.Drawing.Point(1215, 244);
            this.testBugBtn.Name = "testBugBtn";
            this.testBugBtn.Size = new System.Drawing.Size(171, 88);
            this.testBugBtn.TabIndex = 16;
            this.testBugBtn.Text = "testBug";
            this.testBugBtn.UseVisualStyleBackColor = true;
            // 
            // settingsTemp
            // 
            this.settingsTemp.Location = new System.Drawing.Point(1224, 76);
            this.settingsTemp.Name = "settingsTemp";
            this.settingsTemp.Size = new System.Drawing.Size(162, 26);
            this.settingsTemp.TabIndex = 17;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(1224, 128);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(164, 26);
            this.textBox2.TabIndex = 18;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(1231, 367);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(156, 84);
            this.listBox1.TabIndex = 19;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1613, 692);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.settingsTemp);
            this.Controls.Add(this.testBugBtn);
            this.Controls.Add(this.connect2Btn);
            this.Controls.Add(this.connect1Btn);
            this.Controls.Add(this.bugForm2Btn);
            this.Controls.Add(this.textBoxCamera);
            this.Controls.Add(this.videoBtn);
            this.Controls.Add(this.Calibrate);
            this.Controls.Add(this.textBoxPortStatus);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.deviceList);
            this.Controls.Add(this.connectPort);
            this.Controls.Add(this.statusText);
            this.Controls.Add(this.portsList);
            this.Controls.Add(this.refreshPorts);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SettingsForm";
            this.Text = "ROV Control";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer joystickTimer;
        private System.Windows.Forms.Timer serialTimer;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox statusText;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Button Calibrate;
        private System.Windows.Forms.ListBox deviceList;
        private System.Windows.Forms.TextBox textBoxCamera;
        private System.Windows.Forms.Button refreshPorts;
        private System.Windows.Forms.ListBox portsList;
        private System.Windows.Forms.Button connectPort;
        private System.Windows.Forms.TextBox textBoxPortStatus;
        private System.Windows.Forms.Button videoBtn;
        private System.Windows.Forms.Button bugForm2Btn;
        private System.Windows.Forms.Button connect1Btn;
        private System.Windows.Forms.Button connect2Btn;
        private System.Windows.Forms.Button testBugBtn;
        private System.Windows.Forms.TextBox settingsTemp;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListBox listBox1;
    }
}

