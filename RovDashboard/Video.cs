﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.XImgproc;

namespace NorthPauldingRov
{
    public partial class Video : Form
    {
        public Image<Bgr, Byte> img;
        //public Mat img;
        private VideoCapture _capture;
        private Mat _frame;
        private List<Point> centers = new List<Point>();
        bool stopped;
        //TextBox settingsTemp;
        //double avgArea;

        private void test(object sender, EventArgs e)
        {
            stopped = false;
        }
        
        private void ProcessFrame(object sender, EventArgs e)
        {
            if (_capture != null && _capture.Ptr != IntPtr.Zero && !stopped)
            {
                _capture.Retrieve(_frame, 0);
                imageBox1.Image = _frame;
            }
           /* lock (settingsTemp)
            {
                settingsTemp.Text = "Temp " + settingsTemp.Text;
            }*/
        }

        public Video(/*TextBox textBox1*/)
        {
            //settingsTemp = tempTextTrue;
            InitializeComponent();
            stopped = false;
            _capture = new VideoCapture(1);

            _capture.ImageGrabbed += ProcessFrame;
            _frame = new Mat();
            if (_capture != null)
            {
                try
                {
                    _capture.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Video_Load(object sender, EventArgs e)
        {

        }

        private void button1_ButtonClick(object sender, EventArgs e)
        {
            if (button1.Text == "Freeze")
            {
                _capture.Stop();

                //CIRCLES
                Image<Bgr, Byte> img = new Image<Bgr, Byte>(imageBox1.Image.Bitmap);
                UMat uimage = new UMat();
                CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);
                
                img = uimage.ToImage<Bgr, Byte>();
                img = img.PyrDown().PyrUp().SmoothMedian(5);
                //CvInvoke.GaussianBlur(img, img, Size(5, 5), 0);
                                   
                stopped = true;

                double resolution = 2.0;
                double minDistance = 20.0;
                int cannyThreshold = 180;
                int accumulatorThreshold = 160; //lower the # the more false circles detected
                                               //90 works well for prop
                int minRadius = 5;
                int maxRadius = 500;

                //Image<Bgr, Byte> img2 = new Image<Bgr, byte>(img.Bitmap);

                //apply OpenCV Hough Transform Circle Detection Algorithm
                Color cannyColor = Color.FromArgb(1, cannyThreshold, cannyThreshold, cannyThreshold);
                Color threshold = Color.FromArgb(1, accumulatorThreshold, accumulatorThreshold, accumulatorThreshold);
                CircleF[][] circles = img.HoughCircles(new Bgr(cannyColor), new Bgr(threshold), resolution, minDistance, minRadius, maxRadius);

                //Draw the circles on the image
                for (int i = 0; i < circles[0].Length; i++)
                {
                    Point center = new Point((int)circles[0][i].Center.X, (int)circles[0][i].Center.Y);
                    CvInvoke.Circle(img, center, (int)circles[0][i].Radius, new MCvScalar(255, 0, 0), 3);
                    centers.Add(center);
                }

                circle.Text = "" + circles[0].Length;

                //imageBox1.Image = img2;

                UMat cannyEdges; //New image to hold output from edge detection algorithm

                //setting for the Canny edge detection algorithm
                double threshold2 = 180.0;
                double thresholdLinking = 120.0;

                //apply OpenCV canny edge detection algorithm
                cannyEdges = img.Canny(threshold2, thresholdLinking).ToUMat();

                //imageBox4.Image = cannyEdges.ToImage<Bgr, Byte>();

                VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint(); //Variable to hold output from the contour detection algorithm

                //apply OpenCV contour detection algorithm on the edge-detected image  Note: this aglorithm will find two copies of each shape (outer and inner)
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxNone);

                Image<Bgr, Byte> img3 = new Image<Bgr, byte>(img.Bitmap);

                for (int i = 0; i < contours.Size; i++)
                {
                    for (int j = 0; j < contours[i].Size; j++)
                    {
                        CvInvoke.Circle(img3, new Point(contours[i][j].X, contours[i][j].Y), 1, new MCvScalar(100, 100, 250), 1);
                    }
                }

                List<Triangle2DF> triangles = new List<Triangle2DF>();
                List<RotatedRect> rectangles = new List<RotatedRect>();
                List<RotatedRect> lines = new List<RotatedRect>();

                List<List<Point>> cornerList = new List<List<Point>>();

                for (int i = 0; i < contours.Size; i++)
                {
                    VectorOfPoint approxContour = new VectorOfPoint();
                    CvInvoke.ApproxPolyDP(contours[i], approxContour, CvInvoke.ArcLength(contours[i], true) * 0.04, true);

                    List<Point> corners = new List<Point>();

                    corners.Add(approxContour[0]);
                    bool tooClose = false;

                    int xSum = 0;
                    int ySum = 0;

                    for (int k = 0; k < approxContour.Size; k++)
                    {
                        xSum += approxContour[k].X;
                        ySum += approxContour[k].Y;
                    }

                    Point newCenter = new Point(xSum / approxContour.Size, ySum / approxContour.Size);

                    for (int k = 0; k < centers.Count; k++)
                    {
                        int x = newCenter.X;
                        int y = newCenter.Y;
                        int x2 = centers[k].X;
                        int y2 = centers[k].Y;
                        double dist = Math.Sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y));     //distance formula
                        double ratio = dist / imageBox1.Image.Size.Width;
                        Console.WriteLine("Polygon: " + i + " Corners: " + approxContour.Size + " Distance: " + dist + " Ratio: " + ratio);
                        if (ratio < .15)       //ratio to see if to close
                            //o.15 is best
                        {
                            tooClose = true;
                        }
                    }

                    if (tooClose == false)
                    {
                        for (int j = 0; j < approxContour.Size; j++)
                        {          //adjusting from j = 1 to j = 0

                            int x1 = approxContour[j].X;
                            int y1 = approxContour[j].Y;
                            bool tooClose2 = false;

                            for (int k = 0; k < corners.Count; k++)
                            {
                                int x2 = corners[k].X;
                                int y2 = corners[k].Y;

                                double dist = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                                if (dist < 10)       //adjust the distance at which corners get removed
                                {
                                    tooClose2 = true;
                                }
                            }
                            if (!tooClose2)
                            {
                                corners.Add(approxContour[j]);
                            }
                        }
                    }
                    cornerList.Add(corners);

                    for (int j = 0; j < corners.Count; j++)
                    {
                        CvInvoke.Circle(img3, new Point(corners[j].X, corners[j].Y), 2, new MCvScalar(255, 0, 0), 2);
                    }



                    if (corners.Count == 3)
                    {
                        triangles.Add(new Triangle2DF(corners[0], corners[1], corners[2]));
                    }

                    if (corners.Count == 4)
                    {
                        Point[] vertices = corners.ToArray();
                        RotatedRect newRectangle = CvInvoke.MinAreaRect(new VectorOfPoint(corners.ToArray()));

                        //Get the width and height of the rectangle
                        float width = newRectangle.Size.Width;
                        float height = newRectangle.Size.Height;

                        //Calculate the ratio of the smaller side to the larger side of the rectangle
                        float ratio = Math.Min(width, height) / Math.Max(width, height);

                        if (ratio > 0.3) //If the smaller side is at least 20% the size of the larger side, then count this as a rectangle
                        {
                            rectangles.Add(newRectangle);
                        }
                        else //otherwise count this as a line
                        {
                            lines.Add(newRectangle);
                        }
                    }
                }

                triangle.Text = "" + (triangles.Count - 1);
                square.Text = "" + rectangles.Count / 2;
                line.Text = "" + lines.Count / 2;

                //imageBox6.Image = img3;

                //Draw the triangles on the image
                for (int i = 0; i < triangles.Count; i++)
                {
                    img.Draw(triangles[i], new Bgr(Color.Green), 3);
                }

                //Draw the rectangles on the image
                for (int i = 0; i < rectangles.Count; i++)
                {
                    img.Draw(rectangles[i], new Bgr(Color.Yellow), 3);
                }

                //Draw the lines on the image
                for (int i = 0; i < lines.Count; i++)
                {
                    img.Draw(lines[i], new Bgr(Color.Red), 3);
                }
                imageBox1.Image = img;
            }
            else
            {
                _capture.Start();
                button1.Text = "Freeze";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _capture.Start();
            stopped = false;
            
        }

    }
}
    