﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SharpDX.DirectInput;
using System.IO.Ports;
using Emgu.CV;
using Emgu.CV.Structure;


namespace NorthPauldingRov
{
    public partial class SettingsForm : Form
    {
        Video videoForm;
        //bugForm2 bug;
        //bugForm bugTest;
        DirectInput directInput = new DirectInput();
        IList<DeviceInstance> devices;

        Joystick controller1;
        Joystick controller2;

        int[] maxs = new int[4];
        int[] mins = new int[4];
        int[] centers = new int[4];

        public int value = new int();
        public int[] value2 = new int[255];

        int[] axes = new int[4];
        bool[] buttons = new bool[7];
        bool lightOn = false;
        bool firstLight = true;

        IList<byte> controlData = new List<byte>();

        const int DEAD_ZONE = 7000;                        //point to pass before the program reads change
        long lightTrack;

        public SettingsForm()
        {
            InitializeComponent();
            videoForm = new Video();
            //videoForm = new Video();
            //bug = new bugForm2();
            //bugTest = new bugForm(); 
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {

        }
        
        private void refreshButton_Click(object sender, EventArgs e)
        {
            devices = directInput.GetDevices();
            deviceList.Items.Clear();
           // deviceList.Items.Add(mainImgRec.GetMessage());
            for (int i = 0; i < devices.Count; i++)
            {
                deviceList.Items.Add(devices[i].ProductName);
            }

           /* int select = deviceList.SelectedIndex;
            if (select != -1)
            {
                if (con == false) {
                    Guid guid = devices[select].InstanceGuid;
                    controller1 = new Joystick(directInput, guid);
                    controller1.Acquire();
                    con = true;
                }
                else {
                    Guid guid = devices[select].InstanceGuid;
                    controller2 = new Joystick(directInput, guid);
                    controller2.Acquire();
                }
            }*/

        }

        private void connect1_Click(object sender, EventArgs e)
        {
            int select = deviceList.SelectedIndex;
            if (select != -1)
            {
                Guid guid = devices[select].InstanceGuid;
                controller1 = new Joystick(directInput, guid);
                controller1.Acquire();
            }
        }

        private void connect2_Click(object sender, EventArgs e)
        {
            int select = deviceList.SelectedIndex;
            if (select != -1)
            {
                Guid guid = devices[select].InstanceGuid;
                controller2 = new Joystick(directInput, guid);
                controller2.Acquire();
            }
        }

        private void joystickTimer_Tick(object sender, EventArgs e)
        {
            if (controller1 == null)
            {
                return;
            }
            try
            {
                JoystickState state1 = controller1.GetCurrentState(); //drive stick
                JoystickState state2 = controller2.GetCurrentState(); //arm stick

                axes[0] = state1.X; 
                axes[1] = state1.Y;
                axes[2] = state2.X;
                axes[3] = state2.Y;

                for(int i = 0; i < axes.Length; i++)
                {
                   
                    if (axes[i] > maxs[i]) { maxs[i] = axes[i]; }
                    if (axes[i] < mins[i]) { mins[i] = axes[i]; }
                    if (Math.Abs(axes[i] - centers[i]) < DEAD_ZONE)
                    {
                        axes[i] = centers[i];
                    }
                }

                buttons[0] = state1.Buttons[9];//rov up        244
                buttons[1] = state1.Buttons[8]; //rov down      255
                buttons[2] = state2.Buttons[1]; //claw close
                buttons[3] = state1.Buttons[10];//lights on 
               // buttons[5] = state1.Buttons[11]; //lights off
                buttons[4] = state2.Buttons[5]; //temperature
                //textBoxCamera.Text = mins[0].ToString() + "," + maxs[0].ToString(); 
                textBoxCamera.Text = axes[1].ToString();
                //Console.WriteLine(buttons[4]);

                if (state1.PointOfViewControllers[0] > 9000)
                {
                    buttons[5] = true;
                    buttons[6] = false;               //  BACKWARD ON HAT
                }
                else if (state1.PointOfViewControllers[0] == 0)
                {
                    buttons[6] = true;
                    buttons[5] = false;                         //  CENTER ON HAT
                }
                else
                {
                    buttons[5] = false;
                    buttons[6] = false;
                }
                //also change state of joystick if needed
                //choose buttons and put # in the array
                //controller1 will be for the motors

                statusText.Text = "connected";
            } catch
            {
                statusText.Text = "disconnected";
            }
        }

        private void Calibrate_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < axes.Length; i++)
            {
                centers[i] = axes[i];
                mins[i] = centers[i] - 1;
                maxs[i] = centers[i] + 1;
            }
        }
        private byte scale(int min, int max, int val, int low, int high) 
        {
            return (byte) (((float)val) / (max - min) * (high - low) + low);
        }
        
        private void serialTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                List<byte> controlData = new List<byte>();

                controlData.Add(scale(mins[0], maxs[0], axes[0], 0, 60));
                controlData.Add(scale(mins[1], maxs[1], axes[1], 61, 121));
                controlData.Add(scale(mins[2], maxs[2], axes[2], 122, 182));
                controlData.Add(scale(mins[3], maxs[3], axes[3], 183, 243));
                
                for (var i = 0; i < buttons.Length; i++)
                {
                    if (buttons[i])                         
                    {
                        controlData.Add((byte)(244 + i));
                    }
                }

                int breakDuration = 1;
                if(buttons[3] == true && DateTime.UtcNow.Second - lightTrack > breakDuration && lightOn == false)       //attempt to make the lights toggle
                {                                                                                                       //need to make sure (buttons[4] = fasle;) actually works
                    lightOn = true;                   
                    lightTrack = DateTime.UtcNow.Second;
                }
                else if(buttons[3] == true && DateTime.UtcNow.Second - lightTrack > breakDuration && lightOn == true)
                {
                    lightOn = false;
                    lightTrack = DateTime.UtcNow.Second;
                    controlData.Add((byte)(255));
                    buttons[4] = false;
                }

                if (!buttons[2])
                {                                                              
                    controlData.Add((byte)(253));
                }
                if (!buttons[0] && !buttons[1])
                {
                    controlData.Add((byte)(254));
                }
                byte[] dataArray = new byte[controlData.Count];
                controlData.CopyTo(dataArray, 0);
                serialPort1.Write(dataArray, 0, dataArray.Length);

                for(var i = 0; i < dataArray.Length; i++)
                {
                    Console.WriteLine(dataArray[i]);
                }
            }
            catch
            {
                serialPort1.Close();
                serialTimer.Enabled = false;
                connectPort.Text = "Connect";
                textBoxPortStatus.Text = "Lost Connection";
            }
        }
       
        private void deviceList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void imageBox1_Click(object sender, EventArgs e)
        {
                     
        }

        private void refreshPorts_Click(object sender, EventArgs e)
        {
            portsList.Items.Clear();
            String[] ports = SerialPort.GetPortNames();
            for (int i = 0; i < ports.Length; i++)
            {
                portsList.Items.Add(ports[i]);
            }
        }

        private void textBoxPortStatus_TextChanged(object sender, EventArgs e)
        {

        }

        private void connectPort_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                connectPort.Text = "Connect";
                serialTimer.Enabled = false;
                textBoxPortStatus.Text = "Serial Port Closed";
            }
            else
            {
                if (portsList.SelectedIndex != -1)
                {

                    serialPort1.PortName = (String)portsList.SelectedItem;
                    try
                    {
                        serialPort1.Open();
                        connectPort.Text = "Disconnect";
                        serialTimer.Enabled = true;
                        textBoxPortStatus.Text = "Serial Port open";
                    }
                    catch
                    {
                        textBoxPortStatus.Text = "Serial Port failed To Open" + serialPort1.PortName;
                    }
                }
                else
                {
                    textBoxPortStatus.Text = "No COM Selected";
                }
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            while (serialPort1.BytesToRead > 0)
            {
                // int value = serialPort1.ReadByte();
                // value = serialPort1.ReadByte();
                //string a = serialPort1.ReadExisting();
                //Console.WriteLine(a);
                listBox1.Text = a;
                //Console.WriteLine( serialPort1.ToString());
                //value = serialPort1.ReadByte();
                //settingsTemp.Text = value.ToString();

                /*lock (settingsTemp)
                {
                    settingsTemp.Text = value.ToString();        //display int in text box (needs to be reviewed by Loomis)
                }*/
            }
        }

        private void captureImageBox_Click(object sender, EventArgs e)
        {

        }

        private void videoBtn_Click(object sender, EventArgs e)
        {
            videoForm.Show();
        }

        private void videoStart(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                videoForm.Show();
            }
        }

        private void bugFormBtn_Click(object sender, EventArgs e)
        {
           //bug.Show(); 
        }


        private void tempText_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyDown(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.G)
            {
                MessageBox.Show("Enter key pressed");
            }
            if (e.KeyChar == 13)
            {
                MessageBox.Show("Enter key pressed");
            }
        }     
    }
}

