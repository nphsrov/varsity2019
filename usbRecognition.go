package main


//#include <windows.h>
import "C"

import (
	"gocv.io/x/gocv"
	"image/color"
	"image"
	"strconv"
	"math"
	"os"
	"fmt"
)

func main() {

	/*Init*/
	triangle, square, line, circle := 0, 0, 0, 0																//Stores shape data
	webcam, _ := gocv.VideoCaptureDevice(1)																		//Setup camera
	window := gocv.NewWindow("Camera")																			//Create camera window
	released := 0;
	output := gocv.NewWindow("output")
	img := gocv.NewMat()																						//Frame buffer	
	background := gocv.IMRead("shape.png", gocv.IMReadColor)
	squareColor := color.RGBA{255, 0, 0, 1}																		//Outline colors
	circleColor := color.RGBA{0, 255, 0, 1}
	lineColor := color.RGBA{0, 0, 255, 1}
	triangleColor := color.RGBA{255, 0, 255, 1}
	blur := image.Point{5,5}																					//Blur value
	avgArea := float64(0)																						//Average polygon area
	
	for(true)	{
	
	for(C.GetAsyncKeyState(0x20) == 0)	{
	
		if(C.GetAsyncKeyState(0x20) == 0)	{
		released = 0;
		}
		
		if(C.GetAsyncKeyState(0x1B) < 0)	{
			fmt.Println("exit")	
			/*Close App*/ 
			window.Close()
			webcam.Close()
			os.Exit(0)
		}
		
		 	webcam.Read(&img)																						//Save frame to buffer
			window.IMShow(img)
			window.WaitKey(1)
	}
		if(released == 0)	{
		
		released = 1;
		
		/*Convert Current Frame*/
		gocv.CvtColor(img, &img, gocv.ColorRGBToGray)																//Convert to grayscale
		gocv.Threshold(img, &img, 100, 255, gocv.ThresholdBinary)													//Convert image to binary
		gocv.GaussianBlur(img, &img , blur, 0, 0, gocv.BorderConstant)												//Apply gaussian blur
		gocv.BitwiseNot(img, &img)																					//Invert image colors
		
		/*Find Contours*/
		contours := gocv.FindContours(img, gocv.RetrievalTree, gocv.ChainApproxNone)								//Detect contours
		out := img.Clone()																							//Create copy of camera frame
		gocv.CvtColor(out, &out, gocv.ColorGrayToBGRA)																//Convert output to RGBA (allows colored outlines)
		
		/*Find Shapes*/
		for i := 0; i < len(contours); i++	{																		//Calculate average area of polygons										
			avgArea += gocv.ContourArea(contours[i])
		}
		avgArea = avgArea / float64(len(contours))
		
		for i := 0; i < len(contours); i++	{
			length := gocv.ArcLength(contours[i], true)																//Approximate the contour perimeter
			coords := gocv.ApproxPolyDP(contours[i], 0.04 * length, true)
			poly := len(coords)		
			
			if(gocv.ContourArea(contours[i]) * 1.2 < avgArea && gocv.ContourArea(contours[i]) > 0.01 * avgArea)	{	//Filter out noise with avgArea
				switch poly	{																						//Draw outlines																		
					case 3:
						gocv.DrawContours(&out, contours, i, triangleColor, 2)
						triangle++
					case 4:
						/*Get average length of sides*/
						avgLen := float64(0)																		//Stores the average side length for the current polygon
						curLen := make([]float64, 4)																//Stores the length of all edges of the current polygon
						
						rect := false																				
						
						/*Coordinates for the current edge*/
						ax := float64(0)
						ay := float64(0)
						bx := float64(0)
						by := float64(0)
		
						for n := 0; n <= 3; n++	{

							/*Select current points*/
							if(n < 3)	{
								ax = float64(coords[n].X)
								ay = float64(coords[n].Y)
								bx = float64(coords[n + 1].X)
								by = float64(coords[n + 1].Y)
							} else	{
								ax = float64(coords[0].X)
								ay = float64(coords[0].Y)
								bx = float64(coords[3].X)
								by = float64(coords[3].Y)
							}
							
							/*Find Length and sum*/
							curLen[n] = math.Sqrt(math.Pow(bx - ax, 2) + math.Pow(by - ay, 2))
							avgLen += curLen[n]
						}
						
						/*Average length*/
						avgLen = avgLen / 4
						
						/*Test for rectangle*/
						for n := 0; n <= 3; n++	{
							if(curLen[n] <= 0.5 * avgLen)	{
								rect = true
							}
						}
						
						/*Draw outline*/
						switch(rect)	{
							case true:
								gocv.DrawContours(&out, contours, i, lineColor, 2)
								line++
							case false:
								gocv.DrawContours(&out, contours, i, squareColor, 2)
								square++
							}
							
					case 2:
						gocv.DrawContours(&out, contours, i, lineColor, 2)
						line++
					default:
						gocv.DrawContours(&out, contours, i, circleColor, 2)
						circle++
				}
			}
		}
																	
		display := gocv.NewWindow("Display")																		//Create output window
	
		/*Draw numbers*/
		gocv.PutText(&background, strconv.Itoa(circle), image.Point{50,50}, gocv.FontHersheyPlain, 4, squareColor, 3)
		gocv.PutText(&background, strconv.Itoa(triangle), image.Point{50,115}, gocv.FontHersheyPlain, 4, squareColor, 3)
		gocv.PutText(&background, strconv.Itoa(line), image.Point{50,180}, gocv.FontHersheyPlain, 4, squareColor, 3)
		gocv.PutText(&background, strconv.Itoa(square), image.Point{50,245}, gocv.FontHersheyPlain, 4, squareColor, 3)
		
		output.IMShow(out)																						//Show output in window
		display.IMShow(background)
		output.WaitKey(1)
		
		/*Reset values*/
		square, circle, line, triangle = 0, 0, 0, 0
		
		background = gocv.IMRead("shape.png", gocv.IMReadColor)
		//display.Close()
		fmt.Println("done")
		
	
	}
	}
}