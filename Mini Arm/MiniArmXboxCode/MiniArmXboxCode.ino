#include <Servo.h>
#include <XBOXRECV.h>

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;

USB usb;
XBOXRECV xbox(&usb);

void setup() {
  servo1.attach(43);
  servo2.attach(33);
  servo3.attach(41);
  servo4.attach(35);
  usb.Init();
}

void loop() {
  usb.Task();
  
  long x1 = xbox.getAnalogHat(LeftHatX, 0); 
  long x2 = xbox.getAnalogHat(LeftHatY, 0); 
  long x3 = xbox.getAnalogHat(RightHatX, 0);
  long x4 = xbox.getAnalogHat(RightHatY, 0);

  int angle1 = (x1 + 32000)/64000.0*180;
  int angle2 = (x2 - 32000)/64000.0*-180;
  int angle3 = (x3 - 32000)/64000.0*-180;
  int angle4 = (x4 - 32000)/64000.0*-180;

  servo1.write(angle1);
  servo2.write(angle2);
  servo3.write(angle3);
  servo4.write(angle4);



}

