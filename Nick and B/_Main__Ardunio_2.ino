//#include "Arduino_I2C_ESC.h"    //blue robotics ESC library, dont know label for include
#include <ESC.h>
#include <Servo.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define SERVO_SPEED 0.15      //Speed @6v - 0.15 sec/60 deg (per hardware specs savox)
#define OPEN_SERVO_SPEED 300  //deg per sec
#define CAMERA_SERVO_SPEED 100
#define VERT_UP_SP 1800
#define VERT_DOWN_SP 1200
#define UP_MIN_ANGLE 120
#define UP_MAX_ANGLE 180
#define CAMERA_MIN_ANGLE 20
#define CAMERA_MAX_ANGLE 160
#define OPEN_MIN_ANGLE 0
#define OPEN_MAX_ANGLE 180
#define ROTATE_MIN_ANGLE -1080 //3 full rotations
#define ROTATE_MAX_ANGLE 1080

#define UP_SERVO_PIN 11
#define OPEN_SERVO_PIN 22     //10
#define ROTATE_SERVO_PIN A6
#define CAMERA_SERVO_PIN 35

#define VERT1_MOTOR_PIN 26       
#define VERT2_MOTOR_PIN 30
#define LEFT_MOTOR_PIN A3
#define RIGHT_MOTOR_PIN 8

#define LIGHT_PIN 3     //find out if the lights are connected to the arduino or not
#define TEMP_PIN 34

//Motors have 3 pins and continous motors have 2 pins. 2 PWM pins/wires and 1 high low pin/wire. No need for the servo library.

#define TEMP_DELAY 3000   //milliseconds
#define MOTOR_STOP 1500
#define SERVO_STOP 90     //degrees
#define MAX_MOTOR 450     //350   //in microsenconds

Servo upServo;
Servo openServo;
Servo rotateServo;
Servo cameraServo;

//ESC library might be the problem, attempting solution 
Servo rightMotor;
Servo leftMotor;
Servo vert1;
Servo vert2;
// ^

int Yee = 1500;

/*ESC leftMotor(LEFT_MOTOR_PIN, 1000, 2000, 500);       //change to Servo and use servo.writeMicroseconds. 
ESC rightMotor(RIGHT_MOTOR_PIN, 1000, 2000, 500);
ESC vert1(VERT1_MOTOR_PIN, 1000, 2000, 500);
ESC vert2(VERT2_MOTOR_PIN, 1000, 2000, 500);
*/
OneWire oneWire(TEMP_PIN);       
DallasTemperature tempProbe(&oneWire);   

float upServoVel;
float upServoAngle;
float upTestAngle;
float rotateServoVel;
float rotateServoAngle;
float vert1Vel;
float vert2Vel;
float cameraServoVel;
float cameraServoAngle;
float openServoVel;
float openServoAngle;

float controlXMini;
float controlYMini;

byte input[8];
byte output[2];

long tempTime;
bool miniMode;

double controlX; // X axis of joystick
double controlY; // y axis of joystick

const int MIN_LOOP_TIME = 0; //minimum milliseconds between loops
const float SERVO_VEL = 400; //degrees per second (Speed @6v - 0.15 sec/60 deg)

int loopTime;
int test;
int test2;
int test3;
unsigned long tick;

void setup() {
  upServo.attach(UP_SERVO_PIN);
  openServo.attach(OPEN_SERVO_PIN);
  rotateServo.attach(ROTATE_SERVO_PIN);
  cameraServo.attach(CAMERA_SERVO_PIN);
  tempProbe.begin();

  //ESC library might be the problem, attempting solution 
  leftMotor.attach(LEFT_MOTOR_PIN);   
  rightMotor.attach(RIGHT_MOTOR_PIN);
  vert1.attach(VERT1_MOTOR_PIN);
  vert2.attach(VERT2_MOTOR_PIN);
  // ^
       
  //angles[0] = CENTER;
  upServoAngle = 90;
  rotateServoAngle = 90; 
  cameraServoAngle = 90; //need to check the starting postion
  openServoAngle = 0;
  tempTime = millis();

  upServo.write(10);  //starting position
/*
  leftMotor.arm();
  rightMotor.arm();
  vert1.arm();
  vert2.arm();
  delay(5000);
*/
  pinMode(LIGHT_PIN, OUTPUT);
  
  Serial.begin(115200);
  Serial1.begin(115200);
  //Serial.setTimeout(20);          //Set timeout for reading bytes (You need to wait this long before sending packets)
  tick = millis(); //tick is what we use to keep time, the interntal clock counts in milliseconds
}

void loop() {
  updateLoopTime();  //funtion. Calculate how long since the last time it looped. Set a minimum delay before it can loop again
  readSerial();      //function
  updateServo();     //function
  updateMotors();
}

int servoVelocity(byte byteVal) {
  return (byteVal%61 - 30)/30.0*SERVO_VEL; 
}

void updateLoopTime() {
  //difference between current loop time and tick( the time the last time calulated)
  loopTime = millis() - tick;
  tick = millis();

  if(loopTime < MIN_LOOP_TIME) {
    delay(MIN_LOOP_TIME - loopTime);
    loopTime = MIN_LOOP_TIME;
  }
}

void readSerial() {                               //do not know how or where to define 
  int numBytes = Serial.available();
  if (numBytes > 0) {
    Serial.readBytes(input, numBytes);          //define input which is in the "buffer" parameter of readBytes
    for (int i = 0; i < numBytes; i++){
      if(input[i]==250){
        if(millis() - tempTime > TEMP_DELAY){    //tempDelay not defined
          readTemp();
          tempTime = millis();
        }
      }
      else if(input[i]==249){
        digitalWrite(LIGHT_PIN, LOW);
      }else if(input[i]==248){
        digitalWrite(LIGHT_PIN, HIGH);
      }else if(input[i]==247){
        openServoVel=OPEN_SERVO_SPEED;
      }else if(input[i]==253){
        openServoVel=-1*OPEN_SERVO_SPEED;
      }else if(input[i]==251){
        cameraServoVel = -1*CAMERA_SERVO_SPEED;        //create variable for miniMode
      }else if(input[i]==252){
        cameraServoVel = CAMERA_SERVO_SPEED;
      }else if(input[i]==245){
        vert1Vel = VERT_DOWN_SP;
        vert2Vel = VERT_DOWN_SP;
      }else if(input[i]==244){
        vert2Vel = VERT_UP_SP;
        vert1Vel = VERT_UP_SP;
      }else if(input[i]==254){
        vert1Vel = MOTOR_STOP;        //why is there motor stop?
        vert2Vel = MOTOR_STOP;
      }else if(input[i] <= 60){
        controlXMini = input[i];
        controlX = input[i]/30.0 - 1;
      }else if(input[i] <= 121){
        controlYMini = input[i];
        controlY = (input[i]%61)/30.0 - 1;
      }else if(input[i] <= 182){
        rotateServoVel = (int)((input[i]%61)/60.0*180);
        
      }else if(input[i] <= 243){
        //upServoVel = servoVelocity(input[i]);
        //upTestAngle = (input[i]%61 - 30)/30.0 * 180;
        
        test = input[i] - 183;
        test2 = test*3;
        test3 = test2 / 6;
        }
      }
    }
  }
}


void updateServo(){
  float seconds = ((float)loopTime)/1000.0;
  openServoAngle += openServoVel*seconds;
  if(openServoAngle < OPEN_MIN_ANGLE){
    openServoAngle = OPEN_MIN_ANGLE;
  }
  if(openServoAngle > OPEN_MAX_ANGLE){
    openServoAngle = OPEN_MAX_ANGLE;
  }
  openServo.write(openServoAngle);
  
  upServoAngle += upServoVel*seconds;
  if(upServoAngle < UP_MIN_ANGLE){
    upServoAngle = UP_MIN_ANGLE;
  }
  if(upServoAngle > UP_MAX_ANGLE){
    upServoAngle = UP_MAX_ANGLE;
  }
   
  upServo.write(upServoAngle);

  cameraServoAngle += cameraServoVel*seconds;
  if(cameraServoAngle < CAMERA_MIN_ANGLE){
    cameraServoAngle = CAMERA_MIN_ANGLE;
  }
  if(cameraServoAngle > CAMERA_MAX_ANGLE){
    cameraServoAngle = CAMERA_MAX_ANGLE;
  }
  cameraServo.write(cameraServoAngle);
  
  float vel = (rotateServoVel - 90)/90.0*SERVO_VEL;
  rotateServoAngle += vel*seconds;
  if(rotateServoAngle < ROTATE_MIN_ANGLE){
    rotateServoVel = SERVO_STOP;
  }
  if(rotateServoAngle > ROTATE_MAX_ANGLE){
    rotateServoVel = SERVO_STOP;
  }
  rotateServo.write(rotateServoVel);  //might need to be rotateServoAngle 
}
void updateMotors(){
  int left =  1500 + controlY*MAX_MOTOR + controlX*20;
  int right =  1500 + controlY*MAX_MOTOR - controlX*20;
  right = (1500 - right) + 1500;
  
  //Check with Mr. Loomis about converting this equation to use writeMicroseconds
  //values between 1000 - 2000
  
  if(miniMode == false){
    vert1.writeMicroseconds(vert1Vel);
    vert2.writeMicroseconds(vert2Vel);
    if(controlY >=1500) {   //was at 0
    leftMotor.writeMicroseconds(left);
    rightMotor.writeMicroseconds(right);
      
    }else{
      leftMotor.writeMicroseconds(right);
      rightMotor.writeMicroseconds(left);   
    }
  }else{
    sendCommunication();
  }    
}  
void readTemp(){
  tempProbe.requestTemperatures();
  int temperature = tempProbe.getTempCByIndex(0);
  Serial.write((byte)temperature);
}

void sendCommunication(){
  output[0] = controlXMini;
  output[1] = controlYMini;
  
  Serial1.write(output, 2);
  while (Serial1.available() > 0){
    float x = Serial1.read();
    Serial.println(x);    
  }
}
