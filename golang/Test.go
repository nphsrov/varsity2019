package main


//#include <windows.h>
import "C"

import (
	"gocv.io/x/gocv"
	"image/color"
	"image"
	"os"
	"fmt"
)

func main() {

	/*Init*/
	webcam, _ := gocv.VideoCaptureDevice(0)												//Setup camera
	window := gocv.NewWindow( "Camera" )												//Create display window
	img := gocv.NewMat()																//Frame buffer	
	squareColor := color.RGBA{255, 0, 0, 1}												//Outline colors
	circleColor := color.RGBA{0, 255, 0, 1}
	lineColor := color.RGBA{0, 0, 255, 1}
	triangleColor := color.RGBA{255, 0, 255, 1}
	blur := image.Point{5,5}															//Blur value
	avgArea := float64(0)																//Average polygon area
	key := C.GetAsyncKeyState(0x0D)
	fmt.Println("Hello, Brighton")
	fmt.Println(key)
	for (C.GetAsyncKeyState(0x0D) == 0)	{
		webcam.Read( &img )																//Save frame to buffer
		
		/*Convert Current Frame*/
		gocv.CvtColor( img, &img, gocv.ColorRGBToGray )									//Convert to grayscale
		gocv.Threshold( img, &img, 100, 255, gocv.ThresholdBinary )						//Convert image to binary
		gocv.GaussianBlur(img, &img , blur, 0, 0, gocv.BorderConstant)					//Apply gaussian blur
		gocv.BitwiseNot( img, &img )													//Invert image colors

		gocv.PutText(
			&img,
			"HI ALL GALS!!!!!",
			image.Point{100, 100},
			1,
			1.0,
			color.RGBA{255, 0, 0, 1},
			5)

		/*Find Contours*/
		contours := gocv.FindContours( img, gocv.RetrievalTree, gocv.ChainApproxNone )	//Detect contours
		out := img.Clone()																//Create copy of camera frame
		gocv.CvtColor( out, &out, gocv.ColorGrayToBGRA )								//Convert output to RGBA (allows colored outlines)
		
		/*Find Shapes*/
		for i := 0; i < len(contours); i++	{											//Calculate average area of polygons										
			avgArea += gocv.ContourArea(contours[i])
		}
		avgArea = avgArea / float64(len(contours))
		
		for i := 0; i < len(contours); i++	{
			length := gocv.ArcLength(contours[i], true)									//Approximate the contour perimeter
			poly := len(gocv.ApproxPolyDP(contours[i], 0.04 * length, true))			//Approximate polygonal curves	
			
			if (gocv.ContourArea(contours[i]) * 1.2 < avgArea && gocv.ContourArea(contours[i]) > 0.01 * avgArea) {	//Filter out noise with avgArea
				switch poly	{																					//Draw outlines																		
					case 3:
						gocv.DrawContours(&out, contours, i, triangleColor, 2)
					case 4:
						gocv.DrawContours(&out, contours, i, squareColor, 2)
					case 2:
						gocv.DrawContours(&out, contours, i, lineColor, 2)
					default:
						gocv.DrawContours(&out, contours, i, circleColor, 2)
				}
			}
		}
		
		window.IMShow(out)																//Show output in window
		window.WaitKey(1)																//Delay 1ms	

		key = C.GetAsyncKeyState(0x0D)
	}
	fmt.Println(key)
	/*Close App*/
	window.Close()	
	webcam.Close()
	os.Exit(0)
}				
					