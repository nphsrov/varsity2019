


/*
 * This is an example of how to transfer bytes using serial
 * 
 * A quick note about the data sent and recieved:
 * 
 * It stores the data as bytes, so the arduino terminal will display it as characters
 * This keeps our data conversions simple and transmissions short.
 * It also makes reading data a real pain, so I would use HTerm to view data when you debug
 * Download: http://www.der-hammer.info/terminal/
 * I used this before to debug some VHDL a while back, so it is safe and MUCH more versitile than the arduino monitor
 * 
 * How to use HTerm:
 * 
 * 1.) Select the COM port and baud rate at the top
 * 2.) Uncheck Ascii and check HEX or DEC (This will show raw data instead of ascii characters)
 * 3.) Select Connect button 
 * 4.) This will spit out any data your device is transmitting
 * 
 * Explanation of the data buffer:
 * 
 * buf temporaraly stores data coming from the other device
 * The data is then transfered to rx_data for permanent storage
 * This is a really simple way to make sure we do not have garbage at the end of our array if not all the data is retrieved
 * 
 * How it works:
 * 
 * Recieving data:
 * 1.) Serial.available is polled once everytime the loop runs.  
 * This keeps the code simple as we do not have to use the M3's interrupt handlers. (If you want to, there are a few useful sources on google)
 * 2.) The program will then try to read 8 bytes and stores them in buf.
 * (If 20ms passes without recieving anything, the program will continue)  This helps prevent it from getting "stuck"
 * You can change this value in the setup routine 
 * (You also NEED to wait longer than the period before you write another 8 bytes, as this is how it seperates the packets)
 * 3.)The routine will transfer the data to a permanent location and clear the buffer.
 * We do this so garbage data will not appear in the output in the event that there are fewer than 8 bytes (The buffer will not be completely overwritten)
 * Obviously, there are far better ways to accomplish this, but this keeps everything simple
 * 
 * Transmitting:
 * This is really simple
 * Use Serial.write(data, len)
 * data is the array you want to send
 * len is the length of the array (how many bytes you need to send)
 * 
 * The beauty of using our method of reading data is that it does not care if you send too few bytes (although dont send too many)
 * It will just set the upper bytes to 0
 * 
 */

#include <SparkFun_TB6612.h>
#define AIN1 0
#define BIN1 1
#define AIN2 2
#define BIN2 3
#define PWMA 5
#define PWMB 6
#define STBY 9
#define SCALE 8.5

const int offsetA = 1;
const int offsetB = 1;

float controlX;
float controlY;
float rightSide;
float leftSide;

Motor motorL = Motor(AIN1, AIN2, PWMA, offsetA, STBY); //left front
Motor motorR = Motor(BIN1, BIN2, PWMB, offsetB, STBY); //left back

uint8_t buf[9];      //Stores 8 bytes of incoming data
uint8_t rx_data[9];  //Permanent location to store recieved data

void setup() {
  Serial.begin(115200);             //Open communication
  Serial.setTimeout(20);          //Set timeout for reading bytes (You need to wait this long before sending packets)
}

void loop() {

  //Wait for serial
  if(Serial.available() > 0)  {

    //Read 8 bytes and store in the buffer
    Serial.readBytes(buf, sizeof(buf));

    //Reset buffer and store data in permanent location (makes sure the buffer does not contain garbage at the next read
    for (int i = 0; i < sizeof(buf); i++)  {
      rx_data[i] = buf[i];
      buf[i] = 0;
    }
  }
  
  //Send data back
  Serial.write(rx_data, sizeof(rx_data));
  updateMotors();
}

void updateMotors(){
  for(int i = 0; i < sizeof (rx_data); i++){
    if(rx_data[i] <= 60){
      controlX = rx_data[i]*SCALE - 255;
    }else if(rx_data[i] <= 121){
      controlY = rx_data[i]*SCALE - 255;            //yet to come up with solution
    }
  leftSide = controlY + controlX*0.2;
  rightSide = controlY - controlX*0.2;

  motorL.drive(leftSide); 
  motorR.drive(rightSide); 
  delay(1000);
  }
}  
/* 4 continous wired tank style
 * 
 * control x 0-60, 0 = fast backwards, 30 = stop, 60 = fast forwards
 * control y is the same as control x, 61-121
 * 
 */
